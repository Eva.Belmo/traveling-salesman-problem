import datetime
import os
import json
import random

# ici on trouve le PATH du dossier du projet pour l'utiliser au cours du generation du
# liste des villes
import time

BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))


# la fonction main a executer
def main():
    nom_fichier: str = os.path.join(BASE_DIR, "cities.json")
    nombre_des_villages: int = int(input('Entrez nombre des villes a generer: '))
    # cree un  dictionnaire avec des clee base sur alphabet
    # A, B, C ... etc
    villages: dict = dict({chr(i + 65): {} for i in range(nombre_des_villages)})
    random.seed()
    for i in range(nombre_des_villages):
        # on cache le nom du village courant ( A ou B ...etc)
        nom_du_village = chr(i + 65)
        # on initialize la valeur du village[nom_village] par un dictionnaire
        # qui a des distance pour les autres villes
        villages[nom_du_village] = dict(
            {chr(x + 65): random.randint(1, 30) for x in range(nombre_des_villages) if x is not nom_du_village})
        # on met la de nom_village vers nom_village a 0
        villages[nom_du_village][nom_du_village] = 0

    villages['nombre_des_villages'] = nombre_des_villages

    # ecrite le resultat sur un fichier JSON
    with open(nom_fichier, 'w') as f:
        f.write(json.dumps(villages, indent=4))


# c'est une facon de dire: Si on execute, run main, si le fichier est importe donc run rien
if __name__ == '__main__':
    main()
