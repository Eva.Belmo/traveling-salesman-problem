import os
import math
import json
import generate_random_cities

# si True, chaque execution donne un nouveau set des villes
GENERATE_ON_NEW_RUN = False
BASE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)))
FILE_NAME = os.path.join(BASE_DIR, "cities.json")

nombre_des_villages: int = 0
villes = {}
score = 0
chemain_final = []


def init():
    global villes, nombre_des_villages

    with open(FILE_NAME) as f:
        contents = f.read()
    villes = json.loads(contents)
    nombre_des_villages = villes['nombre_des_villages']
    del villes['nombre_des_villages']


def algo_glouton_1():
    # on se met sur que le chemain est totalement vide avant de commencer
    # car c'est une  variable global, tout les autres algorithmes vont l'utiliser
    chemain_final.clear()

    # on cache le noeud initial pour ne pas se faire tomber dans une boucle
    start = list(villes.keys())[0]

    # la liste des villages restant a visiter
    restant = list(villes.keys() - start)
    # une variable utiliser pour sauvgarder la cle du chemain le plus court
    key = 0

    # On ajoute le point de depart
    chemain_final.append(start)

    somme = 0

    # tant qu'on a des villes a visiter, BOUCLE
    while len(restant) > 0:
        distance_ville_la_plus_proche = math.inf
        # on boucle sur toutes les villes restantes
        # et on trouve la ville la plus proche
        for k in restant:
            # si la distance est la plus petite (la plus proche)
            # on l'ajoute et on sauvgarde la cle
            if villes[start][k] < distance_ville_la_plus_proche:
                distance_ville_la_plus_proche = villes[start][k]
                key = k
        # a la fin du boucle FOR, on est sur que c'est la ville la plus proche qu'on a
        # dans la variable `key`
        chemain_final.append(key)
        start = key
        restant.remove(key)
        somme += distance_ville_la_plus_proche

    print(f"chemain final: {chemain_final}, somme: {somme}")


def run():
    print("Execution de l'algorithm glouton 1: \n")
    algo_glouton_1()


if __name__ == '__main__':
    if GENERATE_ON_NEW_RUN:
        generate_random_cities.main()

    init()
    run()
